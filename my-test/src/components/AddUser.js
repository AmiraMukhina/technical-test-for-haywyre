import React, {useState, useContext} from 'react';
import {GlobalContext} from "../context/GlobalState";
import {Link, useNavigate} from "react-router-dom";
import {Form, FormGroup, Label, Input, Button} from "reactstrap";

export const AddUser = () => {
    const [name, setName] = useState('');
    const {addUser} = useContext(GlobalContext);
    const history = useNavigate();

    const onSubmit = (e) => {
        e.preventDefault();
        const newUser = {
            name
        }
        addUser(newUser);
        history.push("/");
    }

    const onChange = (e) => {
        setName(e.target.value);
    }

    return (
        <Form onSubmit={onSubmit}>
            <FormGroup>
                <Label>Name</Label>
                <Input
                    type="text"
                    value={name}
                    onChange={onChange}
                    name="name"
                    placeholder="Enter user"
                    required="required"></Input>
            </FormGroup>
            <Button type="submit">Submit</Button>
            <Link to="/" className="btn btn-danger ml-2">Cancel</Link>
        </Form>
    )
}
