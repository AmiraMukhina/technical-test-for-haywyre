import React from 'react';
// import { useState, useContext } from 'react'; import { GlobalContext } from
// "../context/GlobalState"; import { useNavigate } from "react-router-dom";
// import { Button } from 'reactstrap';
import {UserList} from './UserList';
import {AddUser} from './AddUser';
import {RecoilRoot} from 'recoil';
// import PersonAdd from './AddUser'; import PersonList from './Users';

function Home() {
    return (
        <div>
            <RecoilRoot>
                <AddUser/>
                <UserList/>
            </RecoilRoot>

        </div>
    )
}

export default Home