import React from 'react';
import {Link} from "react-router-dom";
import {ListGroup, ListGroupItem, Button} from "reactstrap";
import {userQuery} from '../stores/userStore';
import {useRecoilValueLoadable} from 'recoil';
import userService from '../services/userService';

export const UserList = () => {

    const loadingUsers = useRecoilValueLoadable(userQuery);
    const persons = loadingUsers.contents;
    const removeUser = async (id) => {
        await userService.delete(id)
    };

  console.log(persons);
  return (
    loadingUsers.state === 'hasValue' && (   <ListGroup className="mt-4">
    {persons.length > 0 ? (
      <>
        {persons.map(user => (
          <ListGroupItem className="d-flex" key={user.id}>
              
            <strong>{user.name}</strong>
            <div className="ml-auto">
              <Link to={`/edit/${user.id}`} color="warning" className="btn btn-warning mr-1">Edit</Link>
              <Button onClick={() => removeUser(user.id)} color="danger">Delete</Button>
            </div>
          </ListGroupItem>
        ))}
      </>
    ) : (
        <h4 className="text-center">No Users</h4>
      )}
  </ListGroup> )
 
  )
}
