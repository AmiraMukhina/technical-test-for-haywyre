import React from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Home from './components/Home';
import "bootstrap/dist/css/bootstrap.min.css";

function App() {

    return (
        <div className='App-header '>
            <nav>
                <ul>
                    <li>
                        <a href='/component/Home.js'>Home</a>
                    </li>
                </ul>
            </nav>
            <div>
                <BrowserRouter>
                    <Routes>
                        <Route element={<Home />} path="/"></Route>
                    </Routes>
                </BrowserRouter>
            </div>
        </div>
    )
}

export default App
